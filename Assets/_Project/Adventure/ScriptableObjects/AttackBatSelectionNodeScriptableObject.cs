﻿using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "AttackBatSelectionNode", menuName = "ScriptableObjects/Adventure/AttackBatSelectionNodeScriptableObject", order = 8)]
    public class AttackBatSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            Debug.Log("You have attacked bat!");
        }
    }
}
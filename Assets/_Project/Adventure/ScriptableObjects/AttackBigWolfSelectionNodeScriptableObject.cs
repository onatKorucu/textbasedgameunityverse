﻿using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "AttackBigWolfSelectionNode", menuName = "ScriptableObjects/Adventure/AttackBigWolfSelectionNodeScriptableObject", order = 7)]
    public class AttackBigWolfSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            Debug.Log("You have attacked big wolf!");
        }
    }
}
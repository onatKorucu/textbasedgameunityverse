﻿using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "AttackSmallWolfSelectionNode", menuName = "ScriptableObjects/Adventure/AttackSmallWolfSelectionNodeScriptableObject", order = 6)]
    public class AttackSmallWolfSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            Debug.Log("You have attacked small wolf!");
        }
    }
}
﻿using _Project.Adventure.Scripts;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "SelectionNode", menuName = "ScriptableObjects/Adventure/SelectionNodeScriptableObject", order = 2)]
    public abstract class BaseSelectionNodeScriptableObject : ScriptableObject
    {
        public string selectionText;
        public int targetEncounter;

        private static StoryManager _storyManager;

        protected static StoryManager StoryManager
        {
            get
            {
                if (_storyManager == null)
                {
                    _storyManager = GameObject.FindWithTag("StoryManager").GetComponent<StoryManager>();
                }
                return _storyManager;
            }
        }

        public virtual void HandleSelectionSelected()
        {
            DoSpecificMechanics();

            GoToTargetEncounter();
        }

        protected abstract void DoSpecificMechanics();

        protected virtual void GoToTargetEncounter()
        {
            StoryManager.PresentEncounter(targetEncounter);
        }
    }
}
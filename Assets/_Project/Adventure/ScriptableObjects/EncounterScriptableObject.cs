﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Serialization;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Encounter", menuName = "ScriptableObjects/Adventure/EncounterScriptableObject", order = 1)]
    public class EncounterScriptableObject : ScriptableObject
    {
        public int encounterId;
        public string encounterText;
        public List<BaseSelectionNodeScriptableObject> selectionNodes;
    }
}
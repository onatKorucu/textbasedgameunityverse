﻿using System.Collections.Generic;
using _Project.Animals;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "FightEncounter", menuName = "ScriptableObjects/Adventure/FightEncounterScriptableObject", order = 2)]
    public class FightEncounterScriptableObject : EncounterScriptableObject
    {
        public List<BaseAnimalScriptableObject> enemyList;
    }
}
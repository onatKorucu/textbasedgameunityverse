﻿using _Project.Adventure.Scripts;
using _Project.Player.Scripts;
using _Project.Utilities;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "IncreaseIntelligenceSelectionNode", menuName = "ScriptableObjects/Adventure/IncreaseIntelligenceSelectionNodeScriptableObject", order = 4)]
    public class IncreaseIntelligenceSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            IncreaseIntelligence();
        }

        protected override void GoToTargetEncounter()
        {
            if (PlayerModel.Instance.CurrentExperiencePoint == Constants.MINIMUM_EXPERIENCE_POINT)
            {
                Debug.Log("Inside IF CurrentExperiencePoint is now:" + PlayerModel.Instance.CurrentExperiencePoint);
                StoryManager.PresentEncounter(1);
            }
            else
            {
                StoryManager.PresentEncounter(targetEncounter);
            }
        }

        private void IncreaseIntelligence()
        {
            if (PlayerModel.Instance.CurrentExperiencePoint <= Constants.MINIMUM_EXPERIENCE_POINT)
            {
                return;
            }

            PlayerModel.Instance.Intelligence++;
            
            PlayerModel.Instance.CurrentExperiencePoint--;

            Debug.Log("Intelligence is now:" + PlayerModel.Instance.Intelligence);
            Debug.Log("CurrentExperiencePoint is now:" + PlayerModel.Instance.CurrentExperiencePoint);
        }
    }
}
﻿using _Project.Adventure.Scripts;
using _Project.Player.Scripts;
using _Project.Utilities;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "IncreaseStrengthSelectionNode", menuName = "ScriptableObjects/Adventure/IncreaseStrengthSelectionNodeScriptableObject", order = 3)]
    public class IncreaseStrengthSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            IncreaseStrength();
        }

        protected override void GoToTargetEncounter()
        {
            if (PlayerModel.Instance.CurrentExperiencePoint == Constants.MINIMUM_EXPERIENCE_POINT)
            {
                Debug.Log("Inside IF CurrentExperiencePoint is now:" + PlayerModel.Instance.CurrentExperiencePoint);
                StoryManager.PresentEncounter(1);
            }
            else
            {
                StoryManager.PresentEncounter(targetEncounter);
            }
        }

        private void IncreaseStrength()
        {
            if (PlayerModel.Instance.CurrentExperiencePoint <= Constants.MINIMUM_EXPERIENCE_POINT)
            {
                return;
            }

            PlayerModel.Instance.Strength++;
            
            PlayerModel.Instance.CurrentExperiencePoint--;

            Debug.Log("Strength is now:" + PlayerModel.Instance.Strength);
            Debug.Log("CurrentExperiencePoint is now:" + PlayerModel.Instance.CurrentExperiencePoint);
        }
    }
}
﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Level", menuName = "ScriptableObjects/Adventure/LevelScriptableObject", order = 1)]
    public class LevelScriptableObject : ScriptableObject
    {
        public int levelId;
        public List<EncounterScriptableObject> encounters;
    }
}
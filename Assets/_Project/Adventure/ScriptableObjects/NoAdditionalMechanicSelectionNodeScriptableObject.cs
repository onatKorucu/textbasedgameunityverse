﻿using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "NoAdditionalMechanicSelectionNode", menuName = "ScriptableObjects/Adventure/NoAdditionalMechanicSelectionNode", order = 1)]
    public class NoAdditionalMechanicSelectionNodeScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            
        }
    }
}
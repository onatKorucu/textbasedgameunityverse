﻿using _Project.Player.Scripts;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "RunAwayFromFightSelectionNode", menuName = "ScriptableObjects/Adventure/RunAwayFromFightScriptableObject", order = 5)]
    public class RunAwayFromFightScriptableObject : BaseSelectionNodeScriptableObject
    {
        protected override void DoSpecificMechanics()
        {
            Debug.Log("You have escaped from fight! Your health point is:" + PlayerModel.instance.GetHealthPoint());
        }
    }
}
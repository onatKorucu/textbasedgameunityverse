﻿using System.Collections.Generic;
using UnityEngine;

namespace _Project.Adventure.ScriptableObjects
{
    [CreateAssetMenu(fileName = "Story", menuName = "ScriptableObjects/Adventure/StoryScriptableObject", order = 1)]
    public class StoryScriptableObject : ScriptableObject
    {
        public int storyId;
        public List<LevelScriptableObject> levels;
    }
}
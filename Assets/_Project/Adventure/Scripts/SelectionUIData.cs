﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Adventure.Scripts
{
    public class SelectionUIData
    {
        public GameObject SelectionGameObject { get; set; }
        public TextMeshProUGUI SelectionTextMeshProUGUI { get; set; }
        public Button SelectionButton { get; set; }

        public SelectionUIData(GameObject selectionGameObject, TextMeshProUGUI selectionTextMeshProUGUI, Button selectionButton)
        {
            SelectionGameObject = selectionGameObject;
            SelectionTextMeshProUGUI = selectionTextMeshProUGUI;
            SelectionButton = selectionButton;
        }
    }
}
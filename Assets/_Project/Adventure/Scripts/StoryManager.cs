﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using _Project.Adventure.ScriptableObjects;
using _Project.Player.Scripts;
using _Project.Utilities;
using TMPro;
using Unity.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace _Project.Adventure.Scripts
{
    public class StoryManager : MonoBehaviour
    {
        /*[SerializeField] private TextMeshProUGUI storyTMP;

        public TextMeshProUGUI GetStoryTMP()
        {
            return storyTMP;
        }

        public void SetStoryTMP(TextMeshProUGUI input)
        {
            storyTMP = input;
        }*/
        
        
        [field: SerializeField] private TextMeshProUGUI StoryTMP { get; set; } //C# Property makes things easy.
        
        [field: SerializeField] private GameObject SelectionHolder { get; set; }

        [field: SerializeField] private GameObject SelectionButtonPrefab { get; set; }
        [field: SerializeField] private List<SelectionUIData> Selections { get; set; }
        
        [field: SerializeField] private StoryScriptableObject Story { get; set; }
        [field: SerializeField] private LevelScriptableObject CurrentLevel { get; set; }
        [field: SerializeField] private EncounterScriptableObject CurrentEncounter { get; set; }
        
        public PlayerModel PlayerModel  { get; set; }

        private void Start()
        {
            CreatePlayer();
            
            FillSelectionList();

            PresentStory(Story);
        }

        private void CreatePlayer()
        {
            PlayerModel = PlayerModel.Instance; //Just to make sure we have the player ready after this point.
            PlayerModel.SetHealthPoint(Constants.INITIAL_HEALTH_POINT);
        }

        private void OnDestroy()
        {
            RemoveSelectionListener();
        }

        private void RemoveSelectionListener()
        {
            foreach (SelectionUIData selectionUIData in Selections)
            {
                //selectionUIData.SelectionButton.onClick.RemoveListener(IncreaseStrength);
                
                //TODO: REMOVE LİSTENER
            }
        }

        private void FillSelectionList()
        {
            Selections = new List<SelectionUIData>();

            for (int i = 0; i < Constants.MAXIMUM_SELECTION_COUNT; i++)
            {
                Debug.Log("FillSelectionList for loop");
                GameObject selectionGameObject = Instantiate(SelectionButtonPrefab, SelectionHolder.transform);
                TextMeshProUGUI selectionTextMeshProUGUI = selectionGameObject.GetComponentInChildren<TextMeshProUGUI>();

                Button selectionButton = selectionGameObject.GetComponent<Button>();
                
                SelectionUIData selectionUIData = new SelectionUIData(selectionGameObject, selectionTextMeshProUGUI, selectionButton);
                Selections.Add(selectionUIData);
            }
        }
        
        private void PresentStory(StoryScriptableObject story)
        {
            PresentLevel(story.levels[0]);
        }

        private void PresentLevel(LevelScriptableObject level)
        {
            PresentEncounter(level.encounters[0]);
            CurrentLevel = level;
        }

        public void PresentEncounter(EncounterScriptableObject encounter)
        {
            FillEncounterText(encounter);

            EnableNeededSelectionButtons(encounter);

            DisableUnneededSelectionButtons(encounter);

            CurrentEncounter = encounter;
            
            Debug.Log("CurrentEncounter id is:" + CurrentEncounter.encounterId);
        }
        
        public void PresentEncounter(int encounterId)
        {
            Debug.Log("In PresentEncounter, CurrentEncounter id is:" + CurrentEncounter.encounterId);

            EncounterScriptableObject encounter = FindEncounterWithIdentifier(encounterId);
            
            PresentEncounter(encounter);
        }

        private EncounterScriptableObject FindEncounterWithIdentifier(int id)
        {
            foreach (EncounterScriptableObject encounter in CurrentLevel.encounters)
            {
                if (encounter.encounterId == id)
                {
                    return encounter;
                }
            }

            Debug.LogError("Encounter with Target Encounter id is missing. Id was:" + id);
            return CurrentLevel.encounters[0];
        }
        
        private void FillEncounterText(EncounterScriptableObject encounter)
        {
            StoryTMP.text = encounter.encounterText;
        }
        
        private void EnableNeededSelectionButtons(EncounterScriptableObject encounter)
        {
            for (int i = 0; i < encounter.selectionNodes.Count; i++)
            {
                Selections[i].SelectionGameObject.SetActive(true);
                Selections[i].SelectionTextMeshProUGUI.text = encounter.selectionNodes[i].selectionText;
                Selections[i].SelectionButton.onClick.RemoveAllListeners();
                Selections[i].SelectionButton.onClick.AddListener(encounter.selectionNodes[i].HandleSelectionSelected);
            }
        }

        private void DisableUnneededSelectionButtons(EncounterScriptableObject encounter)
        {
            for (int i = encounter.selectionNodes.Count; i < Constants.MAXIMUM_SELECTION_COUNT; i++)
            {
                Selections[i].SelectionGameObject.SetActive(false);
            }
        }
        
    }
}
﻿using System;

namespace _Project.Animals
{
    [Serializable]
    public class BaseAnimal
    {
        public int Strength { get; set; }
        public int Intelligence { get; set; }

        private int healthPoint;
        public void SetHealthPoint(int input)
        {
            if (input < 0) //Input Validation is the cause we use variable encapsulation.
            {
                return;
            }
            healthPoint = input;
        }

        public int GetHealthPoint()
        {
            return healthPoint;
        }

        public bool IsAlive()
        {
            return healthPoint > 0;
        }
    }
}
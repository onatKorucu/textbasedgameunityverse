﻿using UnityEngine;

namespace _Project.Animals
{
    [CreateAssetMenu(fileName = "BaseAnimal", menuName = "ScriptableObjects/Animals/BaseAnimalScriptableObject", order = 1)]
    public abstract class BaseAnimalScriptableObject : ScriptableObject
    {
        public int strength;
        public int intelligence;
        public int healthPoint;
    }
}
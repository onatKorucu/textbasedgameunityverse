﻿using UnityEngine;

namespace _Project.Animals
{
    [CreateAssetMenu(fileName = "Bat", menuName = "ScriptableObjects/Animals/BatScriptableObject", order = 2)]
    public class BatScriptableObject : BaseAnimalScriptableObject
    {
        public int curseAmount;
    }
}
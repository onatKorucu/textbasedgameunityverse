﻿namespace _Project.Animals
{
    public interface ICurse
    {
        public void Curse(int attackReduceAmount); //Reduces attack of target
    }
}
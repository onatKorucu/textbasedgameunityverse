﻿using UnityEngine;

namespace _Project.Animals
{
    [CreateAssetMenu(fileName = "Wolf", menuName = "ScriptableObjects/Animals/WolfScriptableObject", order = 3)]
    public class Wolf : BaseAnimalScriptableObject
    {
        
    }
}
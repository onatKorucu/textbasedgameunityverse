﻿using UnityEngine;

namespace _Project.Player.ScriptableObjects
{
    [CreateAssetMenu(fileName = "PlayerConfiguration", menuName = "ScriptableObjects/Player/PlayerConfigurationScriptableObject", order = 5)]
    public class PlayerConfigurationScriptableObject : ScriptableObject
    {
        
    }
}
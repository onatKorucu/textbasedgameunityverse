﻿using _Project.Utilities;

namespace _Project.Player.Scripts
{
    public class PlayerModel
    {
        public static PlayerModel instance;
        public static PlayerModel Instance { 
            get {             
                if (instance == null)
                {
                    instance = new PlayerModel();
                }
                return instance; 
            }
        }

        private PlayerModel()
        {
            InitializeCurrentExperiencePoint();
            //TODO: Read from Scriptable Object
        }

        public int Strength { get; set; }
        public int Intelligence { get; set; }

        private int currentExperiencePoint;
        
        public int CurrentExperiencePoint
        {
            get { return currentExperiencePoint;}
            set
            {
                if (value >= 0)
                {
                    currentExperiencePoint = value;
                }
            }
        }
        //[field: SerializeField] private int PlayerHealthPoint { get; set; }

        private void InitializeCurrentExperiencePoint()
        {
            CurrentExperiencePoint = Constants.INITIAL_EXPERIENCE_POINT;
        }
        
        private int healthPoint;
        public void SetHealthPoint(int input)
        {
            if (input < 0) //Input Validation is the cause we use variable encapsulation.
            {
                return;
            }
            healthPoint = input;
        }

        public int GetHealthPoint()
        {
            return healthPoint;
        }
        
        public bool IsAlive()
        {
            return healthPoint > 0;
        }
    }
}
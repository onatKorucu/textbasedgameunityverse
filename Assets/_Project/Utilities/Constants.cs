﻿namespace _Project.Utilities
{
    public static class Constants
    {
        public static int INITIAL_HEALTH_POINT = 100;
        public static int INITIAL_EXPERIENCE_POINT = 10;
        public static int MINIMUM_EXPERIENCE_POINT = 0;
        public static int MAXIMUM_SELECTION_COUNT = 5;
    }
}